const { useState } = React;

const App = () => {
  const [name, setName] = useState("Mr. Rogers");
  return (
    <>
      <h1>Hello, {name}</h1>
      <p>What is your name?</p>
      <input
        type="text"
        value={name}
        onChange={e => setName(e.target.value)}
        placeholder="Enter a name"
      />
    </>
  );
};

ReactDOM.render(<App />, document.body);
