(async function main() {
  const { sayHi } = await import("./lib.js");
  sayHi("Mr. Rogers");
})();
